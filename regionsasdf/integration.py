#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 11:16:09 2022

@author: variaatio
"""
from pathlib import Path

from asdf.resource import DirectoryResourceMapping
from .extension import RegionsExtension

def get_extensions():
    return [RegionsExtension()]

def get_resource_mappings():
    # Get path to schemas directory relative to this file
    schemas_path = Path(__file__).parent / "schemas"
    mapping = DirectoryResourceMapping(
        schemas_path,
        "asdf://gitlab.com/variaatio/regionsasdf/schemas/",
        recursive=True,
        filename_pattern="*.yaml",
        stem_filename=True,
    )
    return [mapping]