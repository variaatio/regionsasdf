#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 11:10:05 2022

@author: variaatio
"""
from asdf.extension import Extension, TagDefinition
from .converters import (PixCoordConverter, EllipsePixelRegionConverter,
                         CirclePixelRegionConverter)
class RegionsExtension(Extension):
     extension_uri = "asdf://gitlab.com/variaatio/regionsasdf/extensions/regions-1.0.0"
     tags = [TagDefinition("asdf://gitlab.com/variaatio/regionsasdf/tags/pixcoord-1.0.0",
                           schema_uri="asdf://gitlab.com/variaatio/regionsasdf/schemas/pixcoord-1.0.0"
                           ),
             TagDefinition("asdf://gitlab.com/variaatio/regionsasdf/tags/ellipsepixelregion-1.0.0",
                           schema_uri="asdf://gitlab.com/variaatio/regionsasdf/schemas/ellipsepixelregion-1.0.0"),
             TagDefinition("asdf://gitlab.com/variaatio/regionsasdf/tags/circlepixelregion-1.0.0",
                           schema_uri="asdf://gitlab.com/variaatio/regionsasdf/schemas/circlepixelregion-1.0.0")
             ]
     converters =[PixCoordConverter(), EllipsePixelRegionConverter(),
                  CirclePixelRegionConverter()
                  ]
