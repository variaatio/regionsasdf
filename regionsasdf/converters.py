#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 11:00:15 2022

@author: variaatio
"""
from asdf.extension import Converter

class PixCoordConverter(Converter):
    tags = ["asdf://gitlab.com/variaatio/regionsasdf/tags/pixcoord-1.0.0"]
    types = ["regions.core.pixcoord.PixCoord"]
    
    def to_yaml_tree(self, obj, tag, ctx):
        return {"xc": obj.x, "yc": obj.y}
        
    def from_yaml_tree(self, node, tag, ctx):
        from regions.core.pixcoord import PixCoord
        return PixCoord(node['xc'], node['yc'])
    
class EllipsePixelRegionConverter(Converter):
    tags = ["asdf://gitlab.com/variaatio/regionsasdf/tags/ellipsepixelregion-1.0.0"]
    types = ["regions.shapes.ellipse.EllipsePixelRegion"]

    def  from_yaml_tree(self, node, tag, ctx):
        from regions.shapes.ellipse import EllipsePixelRegion
        from regions.core.metadata import RegionMeta, RegionVisual
        return EllipsePixelRegion(node['center'],
                                  node['width'],
                                  node['height'],
                                  angle=node['angle'],
                                  meta=RegionMeta(node['meta']),
                                  visual=RegionVisual(node['visual'])
                                  )
    def to_yaml_tree(self, obj, tag, ctx):
        return {'center': obj.center,
                'width': obj.width,
                'height': obj.height,
                'angle': obj.angle,
                'meta': dict(obj.meta),
                'visual': dict(obj.visual)}

class CirclePixelRegionConverter(Converter):
    tags = ["asdf://gitlab.com/variaatio/regionsasdf/tags/circlepixelregion-1.0.0"]
    types = ['regions.shapes.circle.CirclePixelRegion']

    def  from_yaml_tree(self, node, tag, ctx):
        from regions.shapes.circle import CirclePixelRegion
        from regions.core.metadata import RegionMeta, RegionVisual
        return CirclePixelRegion(node['center'],
                                 node['radius'],
                                 meta=RegionMeta(node['meta']),
                                 visual=RegionVisual(node['visual'])
                                 )
    def to_yaml_tree(self, obj, tag, ctx):
        return {'center': obj.center,
                'radius':obj.radius,
                'meta': dict(obj.meta),
                'visual': dict(obj.visual)}